Drone plugin for Dokku
----------------------

Project: https://github.com/progrium/dokku

Installation
------------
```
cd /var/lib/dokku-alt/plugins

git clone https://github.com/pedroaspiazu/dokku-drone-plugin drone
dokku plugins-install
```


Commands
--------
```
$ dokku help

```

TODO
----------
* write ENV -> get / set config. inject to docker run -e xxx
* tests
