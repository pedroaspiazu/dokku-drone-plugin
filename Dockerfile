FROM ubuntu:14.04

# provision image
RUN echo "update system and install needed deps" \
    && apt-get -y -qq --force-yes update \
    && apt-get -y -qq --force-yes install wget libsqlite3-dev \
    && echo "install drone" \
    && cd /tmp \
    && wget http://downloads.drone.io/master/drone.deb \
    && dpkg -i drone.deb \
    && echo "add folder for drone.io db" \
    && mkdir -p /var/lib/drone/ \
    && touch /var/lib/drone/drone.sqlite

# configure container execution
EXPOSE 8080

CMD DRONE_SERVER_PORT=$DRONE_SERVER_PORT DRONE_DATABASE_DRIVER=$DRONE_DATABASE_DRIVER DRONE_DATABASE_DATASOURCE=$DRONE_DATABASE_DATASOURCE DRONE_REGISTRATION_OPEN=$DRONE_REGISTRATION_OPEN DRONE_GITHUB_CLIENT=$DRONE_GITHUB_CLIENT DRONE_GITHUB_SECRET=$DRONE_GITHUB_SECRET /usr/local/bin/droned
